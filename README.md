# CIC抽取滤波器RTL与仿真

## 介绍
使用菜鸟教程的代码，构建了iverilog仿真环境  

## 说明
原始地址https://www.runoob.com/w3cnote/verilog-cic.html  
原教程和源码，使用modelsim仿真  
我编写了iverilog+gtkwave仿真环境  
windows和linux皆可运行  
windows下使用批处理脚本  
linux下使用makefile  

## 使用教程
### windows
1.  进入make目录，执行make.bat进行仿真  
2.  执行clean.bat清理文件  

### linux
1.  `cd ./make`进入目录  
2.  `make`执行makefile，开始仿真  
3.  `make clean`清理文件  
